# Yii2 oauth2 proxy capable client

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
composer require --prefer-dist mikk150/yii2-oauth-google-proxy-client
```

or add

```json
"mikk150/yii2-oauth-google-proxy-client": "^1.0.0"
```

to the `require` section of your composer.json.

Configuration
-------------

```php
    'components' => [
        'authClientCollection' => [
            'class' => yii\authclient\Collection::class,
            'clients' => [
                'google' => [
                    'class' => mikk150\oauth\proxy\Google::class,
                    'apiBaseUrl' => 'https://www.googleapis.com/oauth2/v1',
                    'tokenUrl' => 'https://accounts.google.com/o/oauth2/token',
                    'fakeReturnUrl' => 'http://localhost:8080/oauth2/callback', //endpoint to https://gitlab.com/oauth2-wildcard/nginx-oauth-proxy
                    'authUrl' => 'http://localhost:8080/oauth2/auth', //endpoint to https://gitlab.com/oauth2-wildcard/nginx-oauth-proxy
                    'clientId' => '<your oauth2 client ID>',
                    'clientSecret' => '<your oauth2 client secret>',
                ]
            ]
        ],
    ]
```

PS. FOR THE LOVE OF GOD, DO NOT USE IT ON PRODUCTION!!!
